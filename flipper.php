<?php

class StringFlipper
{

    function flipString($string)
    {
        $string = strtolower($string);
        $last = strlen($string) - 1;
        $result = array();
        for ($i = $last; $i >= 0; --$i)
        {
            $result[] = $this->flipChar($string[$i]);
        }
        return join('', $result);


    }


    function flipChar($char)
    {
        $charn = $this->getUnicode($char);
        if ($charn !== false)
        {
            if (strlen($charn) > 2)
            {
                $hex = substr($charn, 2);
                $dec = hexdec($hex);
                $charn = "&#$dec;";
            }
            return $charn;
        }
        else
        {
            return $char;
        }


    }


    function getUnicode($c)
    {
        if ($c == 'a')
        {
            return '\u0250';
        }
        else if ($c == 'b')
        {
            return 'q';
        }
        else if ($c == 'c')
        {
            return '\u0254';
        }
        else if ($c == 'd')
        {
            return 'p';
        }
        else if ($c == 'e')
        {
            return '\u01DD';
        }
        else if ($c == 'f')
        {
            return '\u025F';
        }
        else if (c == 'g')
        {
            return 'b';
        }
        else if ($c == 'h')
        {
            return '\u0265';
        }
        else if ($c == 'i')
        {
            return '\u0131'; //'\u0131\u0323'
        }
        else if ($c == 'j')
        {
            return '\u0638';
        }
        else if ($c == 'k')
        {
            return '\u029E';
        }
        else if ($c == 'l')
        {
            return '1';
        }
        else if ($c == 'm')
        {
            return '\u026F';
        }
        else if ($c == 'n')
        {
            return 'u';
        }
        else if ($c == 'o')
        {
            return 'o';
        }
        else if ($c == 'p')
        {
            return 'd';
        }
        else if ($c == 'q')
        {
            return 'b';
        }
        else if ($c == 'r')
        {
            return '\u0279';
        }
        else if ($c == 's')
        {
            return 's';
        }
        else if ($c == 't')
        {
            return '\u0287';
        }
        else if ($c == 'u')
        {
            return 'n';
        }
        else if ($c == 'v')
        {
            return '\u028C';
        }
        else if ($c == 'w')
        {
            return '\u028D';
        }
        else if ($c == 'x')
        {
            return 'x';
        }
        else if ($c == 'y')
        {
            return '\u028E';
        }
        else if ($c == 'z')
        {
            return 'z';
        }
        else if ($c == '[')
        {
            return ']';
        }
        else if ($c == ']')
        {
            return '[';
        }
        else if ($c == '(')
        {
            return ')';
        }
        else if ($c == ')')
        {
            return '(';
        }
        else if ($c == '{')
        {
            return '}';
        }
        else if ($c == '}')
        {
            return '{';
        }
        else if ($c == '?')
        {
            return '\u00BF';
        }
        else if ($c == '\u00BF')
        {
            return '?';
        }
        else if ($c == '!')
        {
            return '\u00A1';
        }
        else if ($c == "\'")
        {
            return ',';
        }
        else if ($c == ',')
        {
            return "\'";
        }
        return false;
    }

}

?>
